#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

int stack[50];
int endPointer = 0;

void pushStack(int num);
bool isOperator(char c);
int popStack();
int getStackSize();
int readNotation(string s);
int getMaxStackSize();

int _tmain(int argc, _TCHAR* argv[]) {
	string s;
	while (true) {
		//string s = "55+23+*";
		cin >> s;
		if (s == "end" || s == "stop" || s == "exit") goto stop;
		cout << "INPUT: " << s << endl;
		cout << "OUTPUT: " << readNotation(s) << endl;
	}
stop:
	return 0;
}

int toInteger(char c) {
	return ((int)c) - 48;
}

int getMaxStackSize() {
	return sizeof(stack) / sizeof(int);
}

void pushStack(int num) {
	if (endPointer < getMaxStackSize()) {
		++endPointer;
		stack[endPointer] = num;
	}
	else{
		cout << "ERROR: No room left in stack to add: " << num << "!" << endl;
	}
}

int popStack() {
	int out = -1;
	if (endPointer > 0) {
		out = stack[endPointer];
		--endPointer;
	}
	else{
		cout << "ERROR: No Items left in Stack to pop!" << endl;
	}
	return out;
}

int getStackSize() {
	return endPointer;
}

bool isOperator(char c) {
	return c == '+' || c == '*' || c == '-' || c == '/';
}

int readNotation(string s) { //Reverse Polish Notation
	int op1, op2;
	for (int i = 0; i < s.length(); ++i) {
		if (isOperator(s[i])) {
			if (getStackSize() >= 2) {
				op2 = popStack();
				op1 = popStack();
				switch (s[i]) {
				case '+':
					pushStack(op1 + op2);
					break;
				case '*':
					pushStack(op1 * op2);
					break;
				case '-':
					pushStack(op1 - op2);
					break;
				case '/':
					pushStack(op1 / op2);
					break;
				default:
					cout << "ERROR: INVALID OPERATOR: " << s[i];
				}
			}
		}
		else{ //Assume Integer
			pushStack(toInteger(s[i]));
		}
	}
	return popStack();
}